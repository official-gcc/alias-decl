/* Copyright (C) 2006, 2007, 2008, 2009, 2010 Free Software Foundation, Inc.

This file is part of GCC.

GCC is free software; you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free
Software Foundation; either version 3, or (at your option) any later
version.

GCC is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or
FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
for more details.

You should have received a copy of the GNU General Public License
along with GCC; see the file COPYING3.  If not see
<http://www.gnu.org/licenses/>.  */

/* This file contains the definition of the named integer constants provided
   by the Fortran 2003 ISO_FORTRAN_ENV intrinsic module.  */

/* The arguments to NAMED_INTCST are:
     -- an internal name
     -- the symbol name in the module, as seen by Fortran code
     -- the value it has
     -- the standard that supports this type  */ 

NAMED_INTCST (ISOFORTRANENV_FILE_ATOMIC_INT_KIND, "atomic_int_kind", \
              gfc_default_integer_kind, GFC_STD_F2008)
NAMED_INTCST (ISOFORTRANENV_FILE_ATOMIC_LOGICAL_KIND, "atomic_logical_kind", \
              gfc_default_logical_kind, GFC_STD_F2008)
NAMED_INTCST (ISOFORTRANENV_CHARACTER_STORAGE_SIZE, "character_storage_size", \
              gfc_character_storage_size, GFC_STD_F2003)
NAMED_INTCST (ISOFORTRANENV_ERROR_UNIT, "error_unit", GFC_STDERR_UNIT_NUMBER, \
              GFC_STD_F2003)
NAMED_INTCST (ISOFORTRANENV_FILE_STORAGE_SIZE, "file_storage_size", 8, \
              GFC_STD_F2003)
NAMED_INTCST (ISOFORTRANENV_INPUT_UNIT, "input_unit", GFC_STDIN_UNIT_NUMBER, \
              GFC_STD_F2003)
NAMED_INTCST (ISOFORTRANENV_INT8, "int8", \
              gfc_get_int_kind_from_width_isofortranenv (8), GFC_STD_F2008)
NAMED_INTCST (ISOFORTRANENV_INT16, "int16", \
              gfc_get_int_kind_from_width_isofortranenv (16), GFC_STD_F2008)
NAMED_INTCST (ISOFORTRANENV_INT32, "int32", \
              gfc_get_int_kind_from_width_isofortranenv (32), GFC_STD_F2008)
NAMED_INTCST (ISOFORTRANENV_INT64, "int64", \
              gfc_get_int_kind_from_width_isofortranenv (64), GFC_STD_F2008)
NAMED_INTCST (ISOFORTRANENV_IOSTAT_END, "iostat_end", LIBERROR_END, \
              GFC_STD_F2003)
NAMED_INTCST (ISOFORTRANENV_IOSTAT_EOR, "iostat_eor", LIBERROR_EOR, \
              GFC_STD_F2003)
NAMED_INTCST (ISOFORTRANENV_IOSTAT_INQUIRE_INTERNAL_UNIT, \
              "iostat_inquire_internal_unit", GFC_INQUIRE_INTERNAL_UNIT, \
              GFC_STD_F2008)
NAMED_INTCST (ISOFORTRANENV_NUMERIC_STORAGE_SIZE, "numeric_storage_size", \
              gfc_numeric_storage_size, GFC_STD_F2003)
NAMED_INTCST (ISOFORTRANENV_OUTPUT_UNIT, "output_unit", GFC_STDOUT_UNIT_NUMBER, \
              GFC_STD_F2003)
NAMED_INTCST (ISOFORTRANENV_REAL32, "real32", \
              gfc_get_real_kind_from_width_isofortranenv (32), GFC_STD_F2008)
NAMED_INTCST (ISOFORTRANENV_REAL64, "real64", \
              gfc_get_real_kind_from_width_isofortranenv (64), GFC_STD_F2008)
NAMED_INTCST (ISOFORTRANENV_REAL128, "real128", \
              gfc_get_real_kind_from_width_isofortranenv (128), GFC_STD_F2008)
NAMED_INTCST (ISOFORTRANENV_FILE_STAT_LOCKED, "stat_locked", \
              GFC_STAT_LOCKED, GFC_STD_F2008)
NAMED_INTCST (ISOFORTRANENV_FILE_STAT_LOCKED_OTHER_IMAGE, \
              "stat_locked_other_image", \
	      GFC_STAT_LOCKED_OTHER_IMAGE, GFC_STD_F2008)
NAMED_INTCST (ISOFORTRANENV_FILE_STAT_STOPPED_IMAGE, "stat_stopped_image", \
              GFC_STAT_STOPPED_IMAGE, GFC_STD_F2008)
NAMED_INTCST (ISOFORTRANENV_FILE_STAT_UNLOCKED, "stat_unlocked", \
              GFC_STAT_UNLOCKED, GFC_STD_F2008)

