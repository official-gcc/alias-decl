/* Copyright (C) 2004, 2005, 2007, 2008 Free Software Foundation, Inc.

This file is part of GCC.

GCC is free software; you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free
Software Foundation; either version 3, or (at your option) any later
version.

GCC is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or
FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
for more details.

You should have received a copy of the GNU General Public License
along with GCC; see the file COPYING3.  If not see
<http://www.gnu.org/licenses/>.  */

/* DEFINE_MATH_BUILTIN (CODE, NAME, ARGTYPE)
   NAME	  The name of the builtin
   SNAME  The name of the builtin as a string
   ARGTYPE The type of the arguments.  See f95-lang.c

   Use DEFINE_MATH_BUILTIN_C if the complex versions of the builtin are
   also available.  */
DEFINE_MATH_BUILTIN_C (ACOS,  "acos",   0)
DEFINE_MATH_BUILTIN_C (ACOSH, "acosh",  0)
DEFINE_MATH_BUILTIN_C (ASIN,  "asin",   0)
DEFINE_MATH_BUILTIN_C (ASINH, "asinh",  0)
DEFINE_MATH_BUILTIN_C (ATAN,  "atan",   0)
DEFINE_MATH_BUILTIN_C (ATANH, "atanh",  0)
DEFINE_MATH_BUILTIN   (ATAN2, "atan2",  1)
DEFINE_MATH_BUILTIN_C (COS,   "cos",    0)
DEFINE_MATH_BUILTIN_C (COSH,  "cosh",   0)
DEFINE_MATH_BUILTIN_C (EXP,   "exp",    0)
DEFINE_MATH_BUILTIN_C (LOG,   "log",    0)
DEFINE_MATH_BUILTIN_C (LOG10, "log10",  0)
DEFINE_MATH_BUILTIN_C (SIN,   "sin",    0)
DEFINE_MATH_BUILTIN_C (SINH,  "sinh",   0)
DEFINE_MATH_BUILTIN_C (SQRT,  "sqrt",   0)
DEFINE_MATH_BUILTIN_C (TAN,   "tan",    0)
DEFINE_MATH_BUILTIN_C (TANH,  "tanh",   0)
DEFINE_MATH_BUILTIN   (J0,    "j0",     0)
DEFINE_MATH_BUILTIN   (J1,    "j1",     0)
DEFINE_MATH_BUILTIN   (JN,    "jn",     2)
DEFINE_MATH_BUILTIN   (Y0,    "y0",     0)
DEFINE_MATH_BUILTIN   (Y1,    "y1",     0)
DEFINE_MATH_BUILTIN   (YN,    "yn",     2)
DEFINE_MATH_BUILTIN   (ERF,   "erf",    0)
DEFINE_MATH_BUILTIN   (ERFC,  "erfc",   0)
DEFINE_MATH_BUILTIN   (TGAMMA,"tgamma", 0)
DEFINE_MATH_BUILTIN   (LGAMMA,"lgamma", 0)
DEFINE_MATH_BUILTIN   (HYPOT, "hypot",  1)

/* OTHER_BUILTIN (CODE, NAME, PROTOTYPE_TYPE)
   For floating-point builtins that do not directly correspond to a
   Fortran intrinsic. This is used to map the different variants (float,
   double and long double) and to build the quad-precision decls.  */
OTHER_BUILTIN (CABS,      "cabs",      cabs)
OTHER_BUILTIN (COPYSIGN,  "copysign",  2)
OTHER_BUILTIN (FABS,      "fabs",      1)
OTHER_BUILTIN (FMOD,      "fmod",      2)
OTHER_BUILTIN (FREXP,     "frexp",     frexp)
OTHER_BUILTIN (HUGE_VAL,  "huge_val",  0)
OTHER_BUILTIN (LLROUND,   "llround",   llround)
OTHER_BUILTIN (LROUND,    "lround",    lround)
OTHER_BUILTIN (NEXTAFTER, "nextafter", 2)
OTHER_BUILTIN (ROUND,     "round",     1)
OTHER_BUILTIN (SCALBN,    "scalbn",    scalbn)
OTHER_BUILTIN (TRUNC,     "trunc",     1)
